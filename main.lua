wait(0.1, function()
	if _PingPong == nil then
		_PingPong = {}
		_PingPong["score"] = 0
		_PingPong["deathmode"] = false
	end
end)

local chatGuildEvent = CreateFrame("Frame")
chatGuildEvent:RegisterEvent("CHAT_MSG_GUILD")
chatGuildEvent:SetScript("OnEvent", function(self, event, message, sender, ...)
	if sender ~= UnitName("player") then
		if message == "ping" then
			SendChatMessage("pong", "GUILD", nil, nil)
			
			if _PingPong["deathmode"] == true and UnitInRaid("player") then
				SendChatMessage("Oh non ! J'ai été éliminé par " .. sender .. " ! Vilain !", "GUILD", nil, nil)

				ForceQuit()
			end
		end
		
		if message == "Oh non ! J'ai été éliminé par " .. UnitName("player") .. " ! Vilain !" then
			_PingPong["score"] = _PingPong["score"] + 1
		end
	end
end)

SLASH_PINGPONG1 = "/pingpong"
SlashCmdList["PINGPONG"] = function(message)
	s_message = str_split(message, " ")
	s_command = s_message[1]
	
	if s_command == "deathmode" then
		mode = s_message[2]
		
		if mode == "on" then
			_PingPong["deathmode"] = true
			print("Le deathmode est désormais activé.")
		elseif mode == "off" then
			_PingPong["deathmode"] = false
			print("Le deathmode est désormais désactivé.")
		else
			if _PingPong["deathmode"] == true then
				print("Le deathmode est actuellement activé.")
			else
				print("Le deathmode est actuellement désactivé.")
			end
		end
	elseif s_command == "score" then
		if _PingPong["score"] <= 1 then
			print("Vous avez déconnecté : " .. _PingPong["score"] .. " joueur.")
		else
			print("Vous avez déconnecté : " .. _PingPong["score"] .. " joueurs.")
		end
	end
end